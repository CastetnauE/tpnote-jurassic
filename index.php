<?php
require "vendor/autoload.php";


$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views'); //a mettre en premier 
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);


Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('trad', function($string){
    }));
});

Flight::route('/', function(){
    $data = [
        "dinosaurs" => recupererDino(),
    ];
    Flight::view()->display('dino.twig', $data);
});


Flight::route('/dinosaurs/@slug', function($slug){
    $data = [
        "dinosaurs" => recupererUnDino($slug),
        "random"=> dinoRandom(),
    ];
    Flight::view()->display('descriptionDino.twig', $data);
});

// faire la route de la description


Flight::start();