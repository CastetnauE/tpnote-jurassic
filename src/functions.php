<?php

function recupererDino()
{
    $response = Requests::get('https://medusa.delahayeyourself.info/api/dinosaurs/');
    $dinosaurs = json_decode($response->body);
    return $dinosaurs;
}

function recupererUnDino($slug)
{
    $response = Requests::get('https://medusa.delahayeyourself.info/api/dinosaurs/'.$slug);
    $dinosaurs = json_decode($response->body);
    return $dinosaurs;
}



function dinoRandom()
{
    $dinosaurs = recupererDino();
    $random_keys = array_rand($dinosaurs, 3); // On récupére trois clés aléatoires
    $top_dinosaurs = array();

    for ($compteur=0;$compteur<=2;$compteur++) {
        $top_dinosaurs[] = $dinosaurs [$random_keys[$compteur]];
    }

return $top_dinosaurs;
}